class User {
    constructor(id, name, position, office, age, startDate) {
        this.id= id;
        this.name = name;
        this.position = position;
        this.office = office;
        this.age = age;
        this.startDate = startDate
    }
}

module.exports = User;